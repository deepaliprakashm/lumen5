import React from 'react';

import VideoData from './VideoData';


class VideoPreview extends React.Component {
    componentDidMount() {
        document.fonts.ready.then(() => {
            // for some reason, we need to wait a little longer still...
            window.setTimeout(() => {
                this.beginDraw(this.refs.canvas);
            }, 200);
        });
    }

    beginDraw(canvas) {
        /* const ctx = canvas.getContext('2d');
        const font_size = 10;
        const message = "Your preview here!";
        ctx.font = `${font_size}px "${VideoData.font}"`;
        ctx.fillText(
            message,
            canvas.width / 2 - ctx.measureText(message).width / 2,
            canvas.height / 2 - font_size / 2,
        ); */

        var previewList = []; // saving frames as canvas in here
        var delay = 800; // the speed of the animation (ms)

        function generatePreview() {
            var c = document.createElement("canvas");
            var ctx = c.getContext("2d");
            c.width = 160;
            c.height = 90;
            ctx.drawImage(video, 0, 0, 160, 90);
            previewList.push(c); // store this frame in our list
            if (previewList.length === 1) {
              displayPreview(); // start animating as soon as we got a frame
            }
          }
          // initialises the display canvas, and starts the animation loop
            function displayPreview() {
            var c = document.createElement("canvas");
            var ctx = c.getContext("2d");
            c.width = 160;
            c.height = 90;
            preview.appendChild(c);
            startAnim(ctx); // pass our visible canvas' context
        }
        function startAnim(ctx) {

            var currentFrame = 0; // here is the actual loop
            function anim() {
              ctx.drawImage(previewList[currentFrame], 0, 0); // draw the currentFrame
              // increase our counter, and set it to 0 if too large
              currentFrame = (currentFrame + 1) % previewList.length;
              setTimeout(anim, delay); // do it again in x ms
            }
            anim(); 
          }
          var i = 0;
          var video = document.createElement("video");
          var preview = document.getElementById("preview");
          
          video.addEventListener('loadeddata', function() {
            preview.innerHTML = "";
            video.currentTime = i;
          }, false);
          
          video.addEventListener('seeked', function() {
            var j = video.duration;
            var u = j / 4;
            // now video has seeked and current frames will show
            // at the time as we expect
            generatePreview(i);
            // when frame is captured, increase
            i += u;
          
            // if we are not passed end, seek to next interval
            if (i <= video.duration) {
              // this will trigger another seeked event
              video.currentTime = i;
            } else {
              //      displayFrame(); // wait for all images to be parsed before animating
            }
          }, false);
          
          video.preload = "auto";
          video.src = VideoData.background.webm_url;
    }

    render() {
        return (
            <div className="lefts-border-line manufacturer-panel">
                <h1 className="manufacturer-title">
                    Video Preview
                </h1>
                <hr/>
                <div className="video-container">
                    <div className="aspect-ratio-fixer">
                        <div className="use-aspect-ratio">
                            <div id="preview" ref="canvas" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default VideoPreview;
